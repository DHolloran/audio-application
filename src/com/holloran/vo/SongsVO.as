package com.holloran.vo
{

	public class SongsVO
	{
		public var artist:String;
		public var album:String;
		public var title:String;
		public var fileName:String;
		public var track:String;

		public function SongsVO()
		{
		}
	}
}