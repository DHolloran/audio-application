package com.holloran.views
{
	import com.holloran.managers.RollManager;
	import com.holloran.managers.SliderManager;
	import com.holloran.utils.NumberUtil;
	import com.holloran.vo.SongsVO;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.media.SoundTransform;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.utils.Timer;
	import libs.MusicPlayerBase;

	public class MusicPlayer extends MusicPlayerBase
	{
		private var _music:Sound;
		private var _sch:SoundChannel;
		private var _st:SoundTransform;
		private var _volume:Number;
		private var _songsList:Array;
		private var _vo:SongsVO;
		private var _sndpos:int;
		private var _rPeak:int;
		private var _lPeak:int;
		private var _track:Number;
		private var _filePath:String;
		private var _fileName:String;
		private var _timer:Timer;
		private var _time:String;
		private var _sec:int;
		private var _min:int;
		private var _saveSec:int;
		private var _saveCount:int;

		public function MusicPlayer()
		{
			super ();
			setup ();

		}

		private function setup():void
		{
			//Init Variables
			_track = 0;
			_sec = 0;
			_min = 0;
			_filePath = "assets/music/"
			_saveCount = 0;

			setupMusic ();


			//--------- Buttons ------------
			this.VolumeIcon.gotoAndStop ( 2 );

			//Next Track Button
			this.BtnNextTrack.stop ();
			this.BtnNextTrack.mouseChildren = false;
			this.BtnNextTrack.buttonMode = true;
			BtnNextTrack.addEventListener ( MouseEvent.MOUSE_DOWN, onNext );

			//Pause/Play Button
			this.BtnPlay.BtnPausePlay.stop ();
			this.BtnPlay.BtnPausePlay.mouseChildren = false;
			this.BtnPlay.BtnPausePlay.buttonMode = true;
			BtnPlay.BtnPausePlay.addEventListener ( MouseEvent.MOUSE_DOWN, onPlay );

			//Previous Track Button
			this.BtnPrevTrack.stop ();
			this.BtnPrevTrack.mouseChildren = false;
			this.BtnPrevTrack.buttonMode = true;
			this.BtnPrevTrack.addEventListener ( MouseEvent.MOUSE_DOWN, onPrev );

			//Mute Button
			this.BtnMute.stop ();
			this.BtnMute.mouseChildren = false;
			this.BtnMute.buttonMode = true;
			this.BtnMute.addEventListener ( MouseEvent.MOUSE_DOWN, onMute );

			//Text Fields
			this.TfArtistName.selectable = false;
			this.tfTime.selectable = false;
			this.tfTrack.selectable = false;
			this.TFTrackName.selectable = false;
			this.TFTrackName.maxChars = 32;

			//Slider
			var slider:VolumeSlider;
			slider = new VolumeSlider;
			slider.x = this.width - ( slider.width + 20 )
			slider.y = this.height - ( slider.height + 25 )
			this.addChild ( slider );
			var sm:SliderManager;
			sm = new SliderManager ();
			sm.setUpAssets ( slider.mc_track, slider.mc_handle );
			sm.percent = .2;
			sm.addEventListener ( Event.CHANGE, volumeAdjust );

			//Roll Manager
			var rm1:RollManager = new RollManager ( this.BtnNextTrack );
			var rm2:RollManager = new RollManager ( this.BtnPrevTrack );

			//XML
			var urlLoader:URLLoader = new URLLoader ();
			urlLoader.load ( new URLRequest ( "assets/xml/songs.xml" ));
			urlLoader.addEventListener ( Event.COMPLETE, onParse );

		}

//Mute Button Func
		private function onMute( e:MouseEvent ):void
		{

			if ( BtnMute.currentFrame == 1 )
			{
				_sndpos = _sch.position;
				_sch.stop ();
				this.BtnMute.gotoAndStop ( 2 );
				_st.volume = 0;
			}
			else if ( BtnMute.currentFrame == 2 )
			{

				setupMusic ();
				_sch = _music.play ( _sndpos, 0, _st );
				_sch.addEventListener ( Event.SOUND_COMPLETE, soundComplete );
				this.BtnMute.gotoAndStop ( 1 );
				_st.volume = _sndpos
			}


		}

//Starts Timer
		private function startTimer():void
		{
			_timer = new Timer ( 1000, 9999 );
			_timer.start ();
			_timer.addEventListener ( TimerEvent.TIMER, onTimer );

		}

		private function onTimer( event:TimerEvent ):void
		{
			_sec++ //Adds one every second

			if ( _sec == 60 )
			{
				//changes seconds to minutes
				_min++;
				_sec = 0;
			}

			//Tracks seconds past
			if ( _sec <= 9 )
			{
				this.tfTime.text = _min + ":0" + _sec;
			}
			else
			{
				this.tfTime.text = _min + ":" + _sec;
			}
			_saveSec = _sec

		}

//Pauses Timer
		private function pauseTimer():void
		{
			var cc:int = _timer.currentCount;

			if ( _saveCount == 0 )
			{
				_saveCount++;
			}
			else if ( _saveCount >= 1 )
			{
				_sec = _saveSec + cc;
			}

			_timer.stop ();

		}

		private function resetTimer():void
		{
			_min = 0;
			_sec = 0;
			_timer.stop ();

		}

//Sets up the music
		private function setupMusic():void
		{
			//Music
			if ( _songsList )
			{
				_fileName = _songsList[ _track ].fileName;
				_music = new Sound ();
				_music.load ( new URLRequest ( _filePath + _fileName ));
				_st = new SoundTransform ();
				_st.volume = .2;
				this.addEventListener ( Event.ENTER_FRAME, update );
			}

		}

//Parses the XML and adds values to SongsVO
		private function onParse( e:Event ):void
		{
			_songsList = [];
			var xmlData:XML = XML ( e.target.data );

			for each ( var song:XML in xmlData.song )
			{
				_vo = new SongsVO ();
				_vo.artist = song.artist;
				_vo.album = song.album;
				_vo.title = song.title;
				_vo.fileName = song.fileName;
				_vo.track = song.track;
				_songsList.push ( song );

			}

			//TextFields
			this.TfArtistName.text = _songsList[ 0 ].artist;
			this.tfTime.text = "0:00";
			this.TFTrackName.text = _songsList[ 0 ].title;
			this.tfTrack.text = "Track: " + _songsList[ 0 ].track;

		}

//Left and Right Sound Peaks
		private function update( e:Event ):void
		{
			if ( _sch )
			{
				_rPeak = _sch.rightPeak;
				_lPeak = _sch.leftPeak;
			}

		}

//Updates the Artist and Track textfields
		private function updateTF():void
		{
			if ( _songsList )
			{
				this.TfArtistName.text = _songsList[ _track ].artist;
				this.TFTrackName.text = _songsList[ _track ].title;
				this.tfTrack.text = "Track: " + _songsList[ _track ].track;

				if ( _time )
				{
					this.tfTime.text = _time;
				}



			}

		}

//Resets and Updates song info
		private function resetSong():void
		{
			this.BtnPlay.BtnPausePlay.gotoAndStop ( 2 );
			updateTF ();

			if ( _timer )
			{
				resetTimer ();
			}

			_sec = 0;
			startTimer ();

			if ( _sch )
			{
				_sch.stop ();
			}

			setupMusic ();
			_sch = _music.play ( _sndpos, 0, _st );
			_sch.addEventListener ( Event.SOUND_COMPLETE, soundComplete );

		}

//Controls which volume icon is displayed
		private function changeVolumeIcon():void
		{
			var v:Number = _volume;

			if ( v == 0 )
			{
				//Mute
				this.VolumeIcon.gotoAndStop ( 1 );
			}
			else if ( v >= 0 && v <= 0.25 )
			{
				//Quarter
				this.VolumeIcon.gotoAndStop ( 2 );
			}
			else if ( v >= 0.25 && v <= 0.49 )
			{
				//Half
				this.VolumeIcon.gotoAndStop ( 3 );
			}
			else if ( v >= 0.50 && v <= 0.74 )
			{
				//Three Quarters
				this.VolumeIcon.gotoAndStop ( 4 );
			}
			else if ( v >= .75 && v <= 1 )
			{
				//Full
				this.VolumeIcon.gotoAndStop ( 5 );
			}

		}

//Adjusts Volume
		private function volumeAdjust( e:Event ):void
		{
			var sm:SliderManager = SliderManager ( e.currentTarget );
			_volume = NumberUtil.roundToDecimal ( sm.percent, 2 );
			changeVolumeIcon ();

			if ( this.BtnPlay.BtnPausePlay.currentFrame == 2 )
			{
				_st.volume = _volume;
				_sch.soundTransform = _st;
			}

		}

//Previous Track Func
		private function onPrev( e:MouseEvent ):void
		{
			//Previous Track
			if ( _track > 0 )
			{
				_track--;
				resetSong ();
			}
			else if ( _track <= 0 )
			{
				_track = _songsList.length - 1;
				resetSong ();
			}

		}

//Next Track Func
		private function onNext( e:MouseEvent ):void
		{
			//Next Track
			if ( _track < _songsList.length - 1 && _track >= 0 )
			{
				_track++;
				resetSong ();
			}
			else if ( _track >= _songsList.length - 1 )
			{
				_track = 0;
				resetSong ();
			}

		}

//Play and Pause Track Func
		private function onPlay( e:MouseEvent ):void
		{
			//Play and Pause button state
			setupMusic ();

			if ( BtnPlay.BtnPausePlay.currentFrame == 1 )
			{
				//Plays Music
				updateTF ();
				startTimer ();
				_sch = _music.play ( _sndpos, 0, _st );
				_sch.addEventListener ( Event.SOUND_COMPLETE, soundComplete );
				this.BtnPlay.BtnPausePlay.gotoAndStop ( 2 );
				this.addEventListener ( Event.ENTER_FRAME, update );
			}
			else
			{
				//Pauses Music
				this.removeEventListener ( Event.ENTER_FRAME, update );
				pauseTimer ();
				_sndpos = _sch.position;
				_sch.stop ();
				this.BtnPlay.BtnPausePlay.gotoAndStop ( 1 );
			}

		}

//Plays Next Song
		private function soundComplete( e:Event ):void
		{
			//Next Track
			if ( _track < _songsList.length - 1 && _track >= 0 )
			{
				_track++;
				resetSong ();
			}
			else if ( _track >= _songsList.length - 1 )
			{
				_track = 0;
				resetSong ();
			}

		}

	}
}